
# aula-03

O objetivo é fazer a primeira chamada a uma api utilizando os hooks do react `useState` e `useEffect`.


Requisitos:
- Deverá instalar a biblioteca `axios`

```bash
    npm install axios
```

ref: https://www.npmjs.com/package/axios

- Deverá criar uma uma pasta com o nome `services` e dentro dela um arquivo `api.js` e neste arquivo instanciar o axios passando como `baseUrl` https://api.escuelajs.co/api/v1

ref: https://fakeapi.platzi.com/en/rest/products

- Deverá importar a instancia do axios no component `App` e fazer uma chamada para o endpoint `products` e salvar o resultado da chamada em uma variavel de estado 

- Deverá renderizar todos os produtos em uma lista simples
com os segintes dados: 

    - title
    - price
    - description


### Bonus:

Ao fazer a chamada a api, deverá renderizar um loading e somente após o retorno da chamada ser completa deverá mostar a lista

ref: https://www.npmjs.com/package/react-loading


